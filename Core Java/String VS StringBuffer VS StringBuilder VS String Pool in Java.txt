String VS StringBuffer VS StringBuilder VS String Pool in Java
	-	Java provides 3 classes to represent a sequence of characters: String, StringBuffer & StringBuilder
	-	String class is an immutable class whereas StringBuffer and StringBuilder classes are mutable.
	
					|				StringBuffer								|						StringBuilder
			-------------------------------------------------------------------------------------------------------------------------------------------------
				1.	|	StringBuffer is synchronized i.e. thread safe. It means	|	StringBuilder is non-synchronized i.e. not thread safe. It means two
					|	two threads can't call the methods of StringBuffer		|	threads can call the methods of StringBuilder simultaneously.	
					|	simultaneously.											|	
					|															|	
				2	|	StringBuffer is less efficient than StringBuilder.		|	StringBuilder is more efficient than StringBuffer.
					|															|
				3.	|	StringBuffer was introduced in Java 1.0					|	StringBuilder was introduced in Java 1.5
			-------------------------------------------------------------------------------------------------------------------------------------------------
	
	-	StringBuffer Example:
			StringBuffer buffer=new StringBuffer("hello");  
			buffer.append("java");  
			System.out.println(buffer);
			
	-	StringBuilder Example:
			StringBuilder builder=new StringBuilder("hello");  
			builder.append("java");  
			System.out.println(builder);
		
	