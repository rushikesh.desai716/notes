Steps to Create custom functional Interface:

1.	Create an Interface with a Single Abstract Method (SAM)
2.	Create a class that implements this interface & its SAM and provides the actual implementation of the SAM.
2.	Create a static method in main that takes interface's object and parameter(s) of the SAM as parameters
		-	inside this method's body, call the SAM using interface's object and pass its parameters.
3.	Inside main, create an object of the Anonymous Class that implements the interface and provide the actual implementation of SAM.
4.	From main, call the static method passing object of interface and SAM's parameter(s) as parameters.