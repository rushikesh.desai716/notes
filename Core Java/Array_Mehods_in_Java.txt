Arrays class contains "asList(T... a)" method which creates a list from an Array

List interface contains "toArray(T[] a)" & "of(E... e)" methods

toArray(T[] a) method	creats an array from a list
of(E... e) creats an immutable list.

List returned from asList(T... a) method is not resizable but it is mutable.
List created using List.of(E... e) method is immutalbe.

But if you use the keyword "new", at the time of initialization, then the resulting list will be mutable & resizable.