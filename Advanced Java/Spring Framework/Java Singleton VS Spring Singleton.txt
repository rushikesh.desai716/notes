Java Singleton VS Spring Singleton:
	
	Java Singleton:	One object instance per JVM
	
	Spring Singleton:	One object instance per Spring IoC Container.